<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Epsilon\Services\Connector as EpsilonConnector;
use App\Epsilon\Exception\EpsilonConfigException;

class EpsilonConnectorTest extends TestCase
{

    public function test_epsilon_api_is_accesable(): void
    {
        $base_url = config('services.epsilon-connector.base_url', 'invalid url');
        $response = $this->get($base_url);

        $response->assertStatus(200);
    }

    public function test_epsilon_credentials_are_valid(): void
    {
        $epsilon = new EpsilonConnector(config('services.epsilon-connector', []));
        $response = $epsilon->fetchToken();
        $this->assertTrue($response !== false);
    }
}
