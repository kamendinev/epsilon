<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ECNetworkService extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $array = parent::toArray($request);
        if (!empty($array['id'])) {
            $array['url'] = route('services.show', ['service' => $array['id']]);
        }
        if (!empty($array['port'])) {
            $array['port'] = new ECPort($array['port']);
        }
        if (!empty($array['b_port'])) {
            $array['b_port'] = new ECPort($array['b_port']);
        }
        return $array;
    }
}
