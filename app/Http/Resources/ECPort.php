<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ECPort extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $array = parent::toArray($request);
        if (!empty($array['id'])) {
            $array['url'] = "Not Implemented Yet ({$array['id']})";
            // $array['url'] = route('ports.show', ['ports' => $array['id']]);
        }
        if (!empty($array['datacentre_logo'])) {
            //Please use full url addreses in api's :(
            $array['datacentre_logo'] = config('services.epsilon-connector.base_url', '/') . $array['datacentre_logo'];
            //Fail again :) can't use the link to show the image without downloading it
            //TODO copy the image assets so they could be used in the frontend.
        }
        return $array;
    }
}
