<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

//I have not used Resource "collections" so far.
//They dont work as I expected so a bit more testing/reading is needed.
class ECNetworkServiceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        $array = parent::toArray($request);
        // $array['url'] = route('services.index'); //this goes to the data array instead of the main level where i expect pagination links to be
        // Don't want to figure them out now.
        return $array;
    }
}
