<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Epsilon\Exceptions\EpsilonException;
use App\Epsilon\Services\Connector as EpsilonConnector;

use App\Http\Resources\ECNetworkService;
use App\Http\Resources\ECNetworkServiceCollection;

class ECNetworkServiceController extends Controller
{
    protected $_epsilon;

    public function __construct(EpsilonConnector $epsilon) {
        $this->_epsilon = $epsilon;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        //TODO handle pagination, filters, sorting?
        $collection = [];

        try {
            $result = $this->_epsilon->get('/api/services');
            $collection = $result->get('services', []);
            return new ECNetworkServiceCollection($collection);
        }
        catch (EpsilonException $err) {
            //TODO add a message to the message buss
            return (new ECNetworkServiceCollection([]))->additional([
                'error' => true,
                'message' => $err->getMessage(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $item = [];
        try {
            $result = $this->_epsilon->get("/api/services/{$id}/service");
            $item = $result->body;
        }
        catch (EpsilonException $err) {
            //TODO add a message to the message buss
            return (new ECNetworkService([]))->additional([
                'error' => true,
                'message' => $err->getMessage(),
            ]);
        }

        return new ECNetworkService($item);
    }

    /**
     * Show the form for editing the specified resource.
     */
    // public function edit(string $id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request, string $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(string $id)
    // {
    //     //
    // }
}
