<?php

namespace App\Epsilon\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Epsilon\Models\OAuthToken;
use App\Epsilon\Models\HTTPResponse;
use App\Epsilon\Exceptions\EpsilonConfigException;
use App\Epsilon\Exceptions\EpsilonHTTPException;


class Connector
{
    const CACHE_ACCESS_TOKEN_KEY = "EC_ACCES_TOKEN";
    const CACHE_KEY_TEMPLATE = "EC_GET:{id}";
    protected OAuthToken $_token;
    protected $_base_url = '';
    protected $_client_id = '';
    protected $_client_secret = '';
    protected $_cahce_duration = 0;

    public function __construct(array $config)
    {
        if (!empty($config['base_url'])) {
            $this->_base_url = $config['base_url'];
        } else {
            throw new EpsilonConfigException('Missing Base URL');
        }

        if (!empty($config['client_id'])) {
            $this->_client_id = $config['client_id'];
        } else {
            throw new EpsilonConfigException('Missing Client ID');
        }

        if (!empty($config['client_secret'])) {
            $this->_client_secret = $config['client_secret'];
        } else {
            throw new EpsilonConfigException('Missing Client Secret');
        }

        if (!empty($config['cache_duration'])) {
            $this->_cahce_duration = intval($config['cache_duration']);
        }

        //It might be good to save the token somwhere safer than the cache
        if ($token = Cache::get(self::CACHE_ACCESS_TOKEN_KEY)) {
            $this->_token = new OAuthToken($token);
        }
    }

    protected function _fetch($config): HTTPResponse
    {
        $cid = str_replace(self::CACHE_KEY_TEMPLATE, '{id}', $config->url);
        if (Cache::has($cid)) {
            return new HTTPResponse(Cache::get($cid));
        }

        if (!empty($config->auto_token) and empty($this->_token) and !$this->fetchToken()) {
            throw new EpsilonConfigException('Failed to get auth token');
        }

        $headers = !empty($config->headers) ? $config->headers : [];
        $headers['Cache-Control'] = 'no-cache';
        $headers['Accept'] = 'application/vnd.cloudlx.v1+json';
        $http = Http::withHeaders($headers);

        if (!empty($this->_token)) {
            $http = $http->withToken($this->_token->access_token);
        }

        switch ($config->method) {
            case 'GET':
                $response = $http->get($config->url);
                break;
            case 'POST':
                $response = $http->post($config->url, $config->data);
                break;
            default:
                throw new EpsilonHTTPException('Unsuported method');
        }

        $code = intval($response->getStatusCode());
        $body = $response->getBody()->getContents();
        $body = empty($body) ? [] : json_decode($body, true);

        if (401 === $code && $newConfig = $this->refreshToken($config)) {
            return $this->_fetch($newConfig); //try again
        }

        $result = new HTTPResponse([
            'code' => $code,
            'body' => $body,
            'headers' => $response->getHeaders(),
            // 'psr7' => $result, // just for debuging harder for caching
        ]);

        if ('GET' === $config->method and !empty($this->_token) and $this->_cahce_duration > 0) {
            Cache::put($cid, $result->toArray(), $this->_cahce_duration);
        }

        return $result;
    }

    public function getToken()
    {
        return $this->_token;
    }

    public function fetchToken(): OAuthToken|false
    {
        unset($this->_token); //this will also remove the barer header
        $response = $this->_fetch((object) [
            'method' => 'POST',
            'url' => "{$this->_base_url}/api/oauth2/access-token",
            'data' => [
                'client_id' => $this->_client_id,
                'client_secret' => $this->_client_secret,
                'grant_type' => 'client_credentials',
            ]
        ]);

        if (200 === $response->code) {
            $this->_token = new OAuthToken($response->body);
            Cache::set(self::CACHE_ACCESS_TOKEN_KEY, $this->_token->toArray());
            return $this->_token;
        } else {
            Log::debug('Failed to get auth token', $response->toArray());
        }

        return false;
    }

    public function refreshToken($config)
    {
        if (empty($this->_token)) {
            return false;
        }
        if (empty($config->auto_refresh)) {
            return false;
        }
        $config->auto_refresh = false;

        $response = $this->_fetch((object) [
            'method' => 'POST',
            'url' => "{$this->_base_url}/api/oauth2/refresh-token",
            'data' => [
                'client_id' => $this->_client_id,
                'client_secret' => $this->_client_secret,
                'grant_type' => 'refresh_token',
                'refresh_token' => $this->_token->refresh_token,
            ]
        ]);

        if (200 === $response->code) {
            $this->_token = new OAuthToken($response->body);
            Cache::set(self::CACHE_ACCESS_TOKEN_KEY, $this->_token->toArray());
            Log::debug('Acces Token Refreshed');
            return $config;
        } else {
            Log::debug('Failed to refresh token', $response->toArray());
        }

        return false;
    }

    public function get(string $url, array $headers = [])
    {
        return $this->_fetch((object) [
            'method' => 'GET',
            'auto_token' => true,
            'auto_refresh' => true,
            'url' => $this->_base_url . $url,
            'headers' => $headers,
        ]);
    }

    public function post(string $url, array $data, array $headers = [])
    {
        return $this->_fetch((object) [
            'method' => 'POST',
            'auto_token' => true,
            'auto_refresh' => true,
            'url' => $this->_base_url . $url,
            'data' => $data,
            'headers' => $headers,
        ]);
    }

    //TODO if needed
    public function put(string $url, array $data, array $headers = [])
    {
    }

    //TODO if needed
    public function patch(string $url, array $data, array $headers = [])
    {
    }

    //TODO if needed
    public function delete(string $url, array $headers = [])
    {
    }

}
