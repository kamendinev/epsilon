<?php

namespace App\Epsilon\Models;

class HTTPResponse
{
    protected $_code = 0;
    protected $_body = [];
    protected $_headers = [];

    function __construct(array $data)
    {
        if (!empty($data['code']))
            $this->_code = $data['code'];
        if (!empty($data['body']))
            $this->_body = $data['body'];
        if (!empty($data['headers']))
            $this->_headers = $data['headers'];
    }

    // for some reason empty($res->body) returns true...
    public function isEmpty() {
        return empty($this->_body);
    }

    public function get($key, $fallback = false) {
        if (empty($this->_body)) {
            return $fallback;
        }
        if (!isset($this->_body[$key])) {
            return $fallback;
        }
        return $this->_body[$key];
    }

    // this is used to get code,body and headers
    public function __get($name)
    {
        $attr = "_" . $name;
        if (!empty($this->$attr)) {
            return $this->$attr;
        }
    }

    public function toArray()
    {
        return [
            'code' => $this->_code,
            'body' => $this->_body,
            'headers' => $this->_headers,
        ];
    }
}
