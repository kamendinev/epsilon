<?php

namespace App\Epsilon\Models;

class OAuthToken
{
    protected $_access_token = '';
    protected $_expires_in = 0;
    protected $_token_type = '';
    protected $_scope = '';
    protected $_refresh_token = '';

    function __construct(array $data)
    {
        if (!empty($data['access_token']))
            $this->_access_token = $data['access_token'];
        if (!empty($data['expires_in']))
            $this->_expires_in = $data['expires_in'];
        if (!empty($data['token_type']))
            $this->_token_type = $data['token_type'];
        if (!empty($data['scope']))
            $this->_scope = $data['scope'];
        if (!empty($data['refresh_token']))
            $this->_refresh_token = $data['refresh_token'];
    }

    public function __get($name)
    {
        $attr = "_" . $name;
        if (!empty($this->$attr)) {
            return $this->$attr;
        }
    }

    public function toArray()
    {
        return [
            'access_token' => $this->_access_token,
            'expires_in' => $this->expires_in,
            'token_type' => $this->token_type,
            'scope' => $this->scope,
            'refresh_token' => $this->refresh_token,
        ];
    }
}
