<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Epsilon\Services\Connector as EpsilonConnector;

class EpsilonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->app->singleton(EpsilonConnector::class, function () {
            return new EpsilonConnector(config('services.epsilon-connector', []));
        });
    }
}
