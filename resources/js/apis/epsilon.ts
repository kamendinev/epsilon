import type { EpsilonResponce, EpsilonError } from "../types/EpsilonApi";
import type { AxiosStatic } from 'axios';
declare var axios: AxiosStatic; //inited in laravel's boostrap.js

class Api {
    protected _env: any;
    protected _base_url: string = '/';
    constructor(env: any) {
        this._env = env;
        if (env.VITE_API_BASE_URL) {
            this._base_url = env.VITE_API_BASE_URL;
            //add trailing slash if needed
            if ('/' !== this._base_url[this._base_url.length - 1]) {
                this._base_url += '/';
            }
        }
    }

    async get(url: string): Promise<EpsilonResponce> {
        let data: any = null;
        let error: EpsilonError | undefined = undefined;
        if ('/' === url[0]) {
            url = url.substring(1); //remove leading slash. Base URL must end with /
        }
        try {
            let result = await axios.get(`${this._base_url}${url}`);
            data = result.data.data;
            if (result.data.error) {
                error = result.data;
            }
            if (data.status_code && 200 !== data.status_code) {
                //for errors that are returned from the epsilon api.
                //this should be handled in the bekend at some point.
                error = data;
                data = null;
            }
        }
        catch (err: any) {
            //not tested properly yet
            error = {
                code: 'not implememnted yet',
                message: err.message
            }
        }

        return { data, error }
    }
}

//create an api singleton;
const api = new Api(import.meta.env);

export const useEpsilonApi = () => {
    return api;
}
