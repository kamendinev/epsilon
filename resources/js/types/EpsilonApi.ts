export interface EpsilonError {
    code: string;
    message: string;
}

export interface EpsilonResponce {
    data: any;
    error?: EpsilonError;
}
