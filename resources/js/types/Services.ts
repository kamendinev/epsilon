export interface ECPort {
    id: number;
    url: string;
    name: string;
    datacentre_datacentre_name: string;
    disabled: boolean;
    status: string;
    error?: boolean;
    message?: string;
}

export interface ECService {
    id: number;
    url: string;
    name: string;
    paused: boolean;
    expired: boolean;
    manual_service: boolean;
    type: string;
    type_short_name: string;
    status: string;
    port: ECPort;
    error?: boolean;
    message?: string;
}
