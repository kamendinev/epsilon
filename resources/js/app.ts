import './bootstrap';
import '../css/app.scss';

import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';

import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'

// Import components
import AppStart from './AppStart.vue';
const WelcomePage = () => import('./Pages/Welcome.vue');
const ServicesList = () => import('./Pages/ServicesList.vue')
const ServicesDetails = () => import('./Pages/ServicesDetails.vue')

const Router = createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', component: WelcomePage },
        { path: '/services', component: ServicesList },
        { path: '/services/:id', component: ServicesDetails },
    ]
});

const app = createApp(AppStart);
app.use(Router);
app.use(Toast);
app.mount('#app');
